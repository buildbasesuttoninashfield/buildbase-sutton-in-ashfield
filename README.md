For all your building materials, timber & DIY needs.
When you trade with Buildbase, you’ll get everything you need in one place. We take pride in serving the local tradespeople and our communities, working with our customers to get the job done, so you can rely on us to give a brilliant service.

Address: The Building Centre, Coxmoor Road, Sutton in Ashfield NG17 4NE, United Kingdom

Phone: +44 1623 515515

Website: [https://www.buildbase.co.uk/storefinder/store/Sutton-in-Ashfield-1421](https://www.buildbase.co.uk/storefinder/store/Sutton-in-Ashfield-1421)
